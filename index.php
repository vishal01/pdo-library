<?php
require 'classes/Database.php';

$database = new Database;

// $database->query('select * from posts WHERE ID = :id');
// $database->bind(':id',1);

$database->query('select * from posts');
$database->bind(':id',1);

$rows = $database->resultset();

//print_r($rows);
if ($_POST) 
{
	$delete_id = $_POST["postid"];
	$database->query('DELETE FROM posts WHERE ID = :id');
    $database->bind(':id',$delete_id);
    $database->execute();

    header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Posts</h2>   
  <a href="create.php"><button type="button" class="btn btn-success">Add Post</button></a>         
  <table class="table table-bordered">
    <thead>
      <tr>
        <th>Title</th>
        <th>Body</th>
        <th>Created Date</th>
        <th>Edit</th>
        <th>Delete</th>
      </tr>
    </thead>
    <tbody>
    <?php foreach ($rows as $row) : ?>
      <tr>
        <td><?php echo $row["title"]; ?></td>
        <td><?php echo $row["body"]; ?></td>
        <td><?php echo $row["created_date"]; ?></td>
        <td><a href="update.php?id=<?php echo $row["ID"]; ?>"><button type="button" class="btn btn-warning">Update</button></a></td>
        <td>
        <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
        <input type="hidden" name="postid" value="<?php echo $row["ID"]; ?>">
        <input type="submit" name="delete" class="btn btn-danger" value="Delete"> 
        </form>
        </td>
      </tr>
    <?php  endforeach; ?>
     
      
    </tbody>
  </table>
</div>

</body>
</html>
