<?php

/**
* 
*/
class Database 
{
	// Database Credentials

	private $host = 'localhost';
	private $user = 'root';
	private $pass = '';
	private $dbname = 'myblog';


	private $dbh;
	private $error;
	private $stmt;


	public function __construct($value='')
	{
		// set DSN
		$dsn = 'mysql:host=' . $this->host . ';dbname=' . $this->dbname;

		$options = array(
			PDO::ATTR_PERSISTENT    => true,
			PDO::ATTR_ERRMODE       => PDO::ERRMODE_EXCEPTION
			);
		     // create new PDO
		     try
		     {
		     	$this->dbh = new PDO($dsn,$this->user,$this->pass,$options);
		     }
		     catch(PDOEception $e)
		     {
		     	$this->error = $e->getMessage();
		     }
	}

    // Query function
	public function query($query)
	{
		$this->stmt = $this->dbh->prepare($query);
	}


	// Bind data
	public function bind($param , $value , $type = null)
	{
		if (is_null($type)) 
		{
			switch (true) 
			{
				case is_int($value):
					$type = PDO::PARAM_INT;
					break;
				case is_bool($value):
				     $type = PDO::PARAM_BOOL;
				     break;
				case is_null($value):
				     $type = PDO::PARAM_NULL;
				     break;
				
				default:
					 $type = PDO::PARAM_STR;
					 break;
			}
		}
		$this->stmt->bindvalue($param , $value , $type);
	}


	// Execute Statement

	public function execute()
	{
		return $this->stmt->execute();
	}


	// last insertId
	public function lastInsertId()
	{
		$this->dbh->lastInsertId();
	}


	// Result set

	public function resultset()
	{
		$this->execute();
		return $this->stmt->fetchAll(PDO::FETCH_ASSOC);
	}





}


?>