<?php
require 'classes/Database.php';

$database = new Database;

$post = filter_input_array(INPUT_POST,FILTER_SANITIZE_STRING);
if ($post["submit"]) 
{
  $title = $post["title"]; echo $title;
  $body = $post["body"];

  $database->query('INSERT INTO posts(title,body) values(:title ,:body)');
  $database->bind(':title',$title);
  $database->bind(':body',$body);
  $database->execute();
  if ($database->lastInsertId()) 
  {
    echo 'Post Added';
  }
  header("Location: index.php");
}
?>
<!DOCTYPE html>
<html lang="en">
<head>
  <title>Bootstrap Example</title>
  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</head>
<body>

<div class="container">
  <h2>Add post</h2>
  <form method="post" action="<?php $_SERVER['PHP_SELF']; ?>">
    <div class="form-group">
      <label for="email">Post Title:</label>
      <input type="text" class="form-control" id="title" name="title" placeholder="Enter Title">
    </div>
    <div class="form-group">
      <label for="pwd">Post Body:</label>
      <textarea class="form-control" id="body" name="body" placeholder="Enter Content"></textarea>  
    </div>
    
    <input type="Submit" name="submit" class="btn btn-success" value="Submit">
  </form>
</div>

</body>
</html>
